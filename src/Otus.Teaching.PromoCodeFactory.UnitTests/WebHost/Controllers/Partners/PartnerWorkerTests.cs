﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers.Services;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers.Services.Abstractions;
using System;
using System.Linq;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerWorkerTests
    {
        private readonly IPartnerLimitWorker _partnerLimitWorker;

        public PartnerWorkerTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnerLimitWorker = fixture.Build<PartnerLimitWorker>().OmitAutoProperties().Create();
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes
        /// </summary>
        [Fact]
        public void SetPartnerPromoCodeLimitAsync_PromocodesIs0_ReturnsTrue()
        {
            // Arrange
            var partner = PartnerBuilder
                .Init()
                .AddSamplePromocodeLimit()
                .SetNumberPromocodes(100)
                .Finish();

            int limit = 100;
            var endDate = DateTime.Now.AddDays(1);

            // Act
            var result = _partnerLimitWorker.WorkWithPartner(partner, limit, endDate);

            // Assert
            result.Should().BeTrue();
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// если лимит закончился, то количество не обнуляется;
        /// </summary>
        [Fact]
        public void SetPartnerPromoCodeLimitAsync_PromocodesIsNot0_ReturnTrue()
        {
            // Arrange
            var partner = PartnerBuilder
                .Init()
                .AddSamplePromocodeLimit()
                .SetNumberPromocodes(100)
                .Finish();
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);
            activeLimit.CancelDate = DateTime.Now;

            var limit = 100;
            var endDate = DateTime.Now.AddDays(1);

            // Act
            var result = _partnerLimitWorker.WorkWithPartner(partner, limit, endDate);

            // Assert
            result.Should().BeTrue();
            partner.NumberIssuedPromoCodes.Should().NotBe(0);
        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит;
        /// </summary>
        [Fact]
        public void SetPartnerPromoCodeLimitAsync_NullPreviousLimit_ReturnsTrue()
        {
            // Arrange
            var partner = PartnerBuilder
                .Init()
                .AddSamplePromocodeLimit()
                .SetNumberPromocodes(100)
                .Finish();
            partner.NumberIssuedPromoCodes = 100;
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            var limit = 100;
            var endDate = DateTime.Now.AddDays(1);

            // Act
            var result = _partnerLimitWorker.WorkWithPartner(partner, limit, endDate);

            // Assert
            activeLimit.CancelDate.Should().NotBeNull();
            result.Should().BeTrue();
        }
    }
}
