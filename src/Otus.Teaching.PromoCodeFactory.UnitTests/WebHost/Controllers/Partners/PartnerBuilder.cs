﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class PartnerBuilder
    {
        private PartnerBuilder() { }

        private Partner _partner;

        public static PartnerBuilder Init()
        {
            var builder = new PartnerBuilder()
            {
                _partner = new Partner { 
                    Id = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165"),
                    Name = "Суперигрушки",
                    IsActive = true,
                    PartnerLimits = new List<PartnerPromoCodeLimit>()
                }
            };
            return builder;
        }

        public PartnerBuilder SetIsActive(bool active)
        {
            _partner.IsActive = active;
            return this;
        }

        public PartnerBuilder SetNumberPromocodes(int number)
        {
            _partner.NumberIssuedPromoCodes = number;
            return this;
        }

        public PartnerBuilder AddSamplePromocodeLimit()
        {
            _partner.PartnerLimits.Add(
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2020, 07, 9),
                    EndDate = new DateTime(2020, 10, 9),
                    Limit = 100
                }
            );
            return this;
        }

        public PartnerBuilder NulleblePromocodeLimit()
        {
            _partner.PartnerLimits = null;
            return this;
        }

        public Partner Finish()
        {
            return _partner;
        }
    }
}
