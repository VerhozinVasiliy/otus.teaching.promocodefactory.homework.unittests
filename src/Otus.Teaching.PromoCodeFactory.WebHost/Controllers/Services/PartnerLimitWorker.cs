﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers.Services.Abstractions;
using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers.Services
{
    public class PartnerLimitWorker : IPartnerLimitWorker
    {
        public bool WorkWithPartner(Partner partner, int limit, DateTime endDate)
        {
            //Установка лимита партнеру
            var activeLimit = partner.PartnerLimits.FirstOrDefault(x =>
                !x.CancelDate.HasValue);

            if (activeLimit != null)
            {
                //Если партнеру выставляется лимит, то мы 
                //должны обнулить количество промокодов, которые партнер выдал, если лимит закончился, 
                //то количество не обнуляется
                partner.NumberIssuedPromoCodes = 0;

                //При установке лимита нужно отключить предыдущий лимит
                activeLimit.CancelDate = DateTime.Now;
            }

            if (limit <= 0)
                return false;

            var newLimit = new PartnerPromoCodeLimit()
            {
                Limit = limit,
                Partner = partner,
                PartnerId = partner.Id,
                CreateDate = DateTime.Now,
                EndDate = endDate
            };

            partner.PartnerLimits.Add(newLimit);

            return true;
        }
    }
}
