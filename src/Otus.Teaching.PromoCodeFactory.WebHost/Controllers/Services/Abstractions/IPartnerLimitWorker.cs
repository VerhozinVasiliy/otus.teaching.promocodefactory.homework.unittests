﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers.Services.Abstractions
{
    public interface IPartnerLimitWorker
    {
        bool WorkWithPartner(Partner partner, int limit, DateTime endDate);
    }
}
